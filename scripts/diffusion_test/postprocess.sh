#!/bin/sh

set -e

for c in 1 1b 2 3 4 5 ; do
    cd $c
    rm -rf postProcess
    ~/ISTEQ/dbnsm/bin/diffusion_ref
    cp ../sample system/
    postProcess -func sample
    cd ..
done

