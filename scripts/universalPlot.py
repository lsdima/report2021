#encoding:utf-8
import numpy as np
from itertools import cycle
import matplotlib as mpl
import matplotlib.pyplot as plt


class sampleDict():
    """
    usage Exmpls:
    if x, y  both are 1D np.arrays with equal sizes,
        SD1=sampleDict(X=x,Y=y)
    or if  data=2D np.array of 2 columns:
        SD2=sampleDict(XY=data)
    or:
        SD3=sampleDict(X=x,Y=y, xLims=[-1,1])
    or:
        SD4=sampleDict(X=x,Y=y, xLims=[-1,1], label='f(x)', color='m', linestyle='--')

        SD4.plot( scaleY='log', toAutoscale=1, title='abcf',legend='best', toSave=1, toShow=1 )
    """
    def __init__(self, **argS):
        if (('X' in argS.keys()) and ('Y' in argS.keys())) \
            and  (isinstance(argS['X'],np.ndarray) and isinstance(argS['Y'],np.ndarray)) \
            and (argS['X'].shape == argS['Y'].shape):
            self.X=argS['X']
            self.Y=argS['Y']
        elif ('XY' in argS.keys()) and isinstance(argS['XY'],np.ndarray) and argS['XY'].shape[1]==2:
            self.X=argS['XY'][:,0]
            self.Y=argS['XY'][:,1]
        else:
            raise Exception("Error: NO XY-data given!")     
        ###############################
        self.xLims=None
        self.xlabel=None
        self.ylabel=None
        self.label=None
        self.color=None
        self.marker=None
        self.LS=None # linestyle wo specifying color
        self.LW=None #linewidth
        self.MS=None
        #################################       
        #
        if ('xLims' in argS.keys()) and isinstance(argS['xLims'],list) and len(argS['xLims'])==2:
            self.xLims=argS['xLims']
            assert self.xLims[0] <self.xLims[1], 'wrong xLims values !'

        if('label' in argS.keys()):
            self.label=argS['label']
        if('xlabel' in argS.keys()):
            self.xlabel=argS['xlabel']
        if('ylabel' in argS.keys()):
            self.ylabel=argS['ylabel']
        #
        if 'color'in argS.keys():
            self.color=argS['color']
        if 'linestyle' in argS.keys():
            self.LS=argS['linestyle']
        elif 'ls' in argS.keys():
            self.LS=args['ls']
        elif 'LS' in argS.keys():
            self.LS=args['LS']

        if 'lw' in  argS.keys():
            self.LW=argS['lw']
        elif'linewidth' in  argS.keys():
            self.LW=argS['linewidth']

        if('marker' in argS.keys()):
            self.marker=argS['marker']

        if('markersize' in argS.keys()):
            self.MS=argS['markersize']
        if('MS' in argS.keys()):
            self.MS=argS['MS']


    def plot(self, **attr): #### attr -> .plot( scaleY='log', toAutoscale=1, title='abcf',legend='best', toSave=1, toShow=1 )
        scaleY=None
        toSave=0 # default Val
        toShow=1 # default Val
        thsTitlE='sampleDict'
        plt.plot(self.X,self.Y, color=self.color, linestyle=self.LS, lw=self.LW, marker=self.marker, markersize=self.MS, label=self.label)
        if not (self.xLims is None):
            plt.xlim((self.xLims[0], self.xLims[1]))
        plt.grid(1)
        # fig.tight_layout()  # otherwise the right y-label is slightly clipped
        if ('scaleY' in attr.keys()):# "linear", "log", "symlog", "logit", 
            plt.yscale(attr['scaleY'])
        #                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        if 'toAutoscale' in attr.keys() and attr['toAutoscale']==1:
            plt.autoscale()

        plt.xlabel(self.xlabel)
        plt.ylabel(self.ylabel)

        plt.xticks(rotation=90)

        if 'legend' in attr.keys():
            plt.legend(loc=attr['legend'], shadow=True) #ex:  'upper left'

        if 'title' in attr.keys():
            thsTitlE=attr['title']
            # plt.title(attr['title'])
        elif not(self.label is None):
            # plt.title(self.label)
            thsTitlE=self.label
        plt.title(thsTitlE)

        if 'toSave' in attr.keys():
            toSave=attr['toSave']
        if toSave!='' or toSave!=0 or toSave!=False:
            if(toSave==1 or toSave==True):
                # plt.savefig(fileName+'.png',format="png")
                plt.savefig(thsTitlE +'.png',format="png")
            else:
                plt.savefig(str(toSave)+'.png',format="png")
            # if toSave:
            # plt.savefig(thsTitlE +'.png',format="png")
        if 'toShow' in attr.keys():
            toShow=attr['toShow']
        if toShow:
            plt.show()
        #
        if 'reset' in attr.keys() and ( not (attr['reset']) or attr['reset']=='No' or attr['reset']=='no'):
            pass
        else:
            plt.clf()# reset 'artists'
            # plt.cla()


        #                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               


def uPlot(*data,**attrb):
    """
    data is a list of sampleDicts
    myData= [SD1,SD2]

    uPlot(myData, title='someName',legend='upper left',toShow=1,toSave=1)

    or:

    uPlot(myData,  title='functiomn(x)',toAutoscale=1)    
    

    """
    # lengths of lists could differ :
    colorCycler=cycle(['#1f77b4','#ff7f0e', '#2ca02c','#d62728','#9467bd', '#8c564b', '#e377c2',  '#7f7f7f', '#bcbd22', '#17becf', '#1a55FF'])
    # colorCycler= cycle( ['c', 'm',  'k','r',   'g'    ])
    lsCycler   = cycle(['-', '--', ':', '-.','solid'])
    lwCycler  =  cycle([ 0.5,   1,  2,   2.5, 1.8 ]) 
    markerCycler =cycle(['o', '+', '^','s','*','h'])
    msCycler    = cycle([5,7])  
    # default_attributes:
    toSave=0
    toShow=1
    toAutoscale=0
    toShareXaxis=0
    fileName='sample'
    xlabel='x'


    N=len(data)
    nSubplots=N
    if 'toShareXaxis' in attrb.keys():
        toShareXaxis=attrb['toShareXaxis']
    if toShareXaxis==1:# plan R : All share one subolot:
        fig, Ax = plt.subplots()
        # Ax.set_prop_cycle(custom_cycler)
        for i in range(N):
            thisColor=data[i].color
            thisLW=data[i].LW
            thisLS=data[i].LS
            thisMarker=data[i].marker
            thisMS=data[i].MS
            #
            if thisColor is None:
                thisColor=next(colorCycler)
            if thisLW is None:
                thisLW=next(lwCycler)
            if thisLS is None:
                thisLS=next(lsCycler)
            if thisMarker is None:
                thisMarker=next(markerCycler)
            if thisMS is None:
                thisMS=next(msCycler)
            #
            Ax.plot(data[i].X,data[i].Y, color=thisColor, linestyle=thisLS, lw=thisLW, marker=thisMarker, markersize=thisMS, label=data[i].label)
            if not  (data[i].xLims is None):
                # ax[i].set_xlim((left=data[i].xLims[0], right=data[i].xLims[1]))
                Ax.set_xlim((data[i].xLims[0], data[i].xLims[1]))
            #
        # if ('log' in attrb.keys()) and attrb['log']:
        #     Ax.set_yscale('log')
        if ('scaleY' in attrb.keys()):
            Ax.set_yscale(attrb['scaleY'])
        #                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        if ('toAutoscale' in attrb.keys()) and attrb['toAutoscale']==1:
            Ax.autoscale()
        #
        Ax.set_xlabel(xlabel)
        #
        Ax.grid(1)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped

        if 'title' in attrb.keys():
            plt.title(attrb['title'])
            fileName=attrb['title']
        if 'legend' in attrb.keys():
            plt.legend(loc=attrb['legend'], shadow=True) #ex:  'upper left'

        fig.subplots_adjust(hspace=0.3)

        if 'toSave' in attrb.keys():
            toSave=attrb['toSave']
        if toSave!='' or toSave!=0 or toSave!=False:
            if(toSave==1 or toSave==True):
                plt.savefig(fileName+'.png',format="png")
            else:
                plt.savefig(str(toSave)+'.png',format="png")
        if 'toShow' in attrb.keys():toShow=attrb['toShow']
        if toShow:
            plt.show()

        #######################################
    else: # column of N subplots (default)   
        fig, ax = plt.subplots(N)
        for i in range(N):
            thisColor=data[i].color
            thisLW=data[i].LW
            thisLS=data[i].LS
            thisMarker=data[i].marker
            thisMS=data[i].MS
            #
            if thisColor is None:
                thisColor=next(colorCycler)
            if thisLW is None:
                thisLW=next(lwCycler)
            if thisLS is None:
                thisLS=next(lsCycler)
            if thisMarker is None:
                thisMarker=next(markerCycler)
            if thisMS is None:
                thisMS=next(msCycler)
            #
            ax[i].plot(data[i].X,data[i].Y, color=thisColor, linestyle=thisLS, lw=thisLW, marker=thisMarker, markersize=thisMS, label=data[i].label)
            #
            if not (data[i].xLims is  None):
                # ax[i].set_xlim((left=data[i].xLims[0], right=data[i].xLims[1]))
                ax[i].set_xlim((data[i].xLims[0], data[i].xLims[1]))
            #
            # if 'log' in attrb.keys():
            # if ('log' in attrb.keys()) and attrb['log'] :
            #     ax[i].set_yscale('log')
            if ('scaleY' in attrb.keys()):
                ax[i].set_yscale(attrb['scaleY'])

            #                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
            if ('toAutoscale' in attrb.keys()) and attrb['toAutoscale']==1:
                ax[i].autoscale()
            #
            ax[i].set_xlabel(data[i].xlabel)        
            ax[i].set_ylabel(data[i].ylabel)
            #
            ax[i].grid(1)
        #######################################
        if 'title' in attrb.keys():
            plt.title(attrb['title'])
            fileName=attrb['title']
        if 'legend' in attrb.keys():
            plt.legend(loc=attrb['legend'], shadow=True) #ex:  'upper left'

        fig.subplots_adjust(hspace=0.3)

        if 'toSave' in attrb.keys():toSave=attrb['toSave']
        if 'toShow' in attrb.keys():toShow=attrb['toShow']
        if toSave:
            plt.savefig(fileName+'.png',format="png")
        if toShow:
            plt.show()


############################################################################################

if __name__ == '__main__':
    # test:
    myX=np.arange(100) +1
    myY=np.log10(myX)
    x1=np.arange(40) +10
    y1=np.abs(np.sin(x1))
    mySD1=sampleDict(X=myX,Y=myY, xLims=[30,71], label='log10(x)', color='m', linestyle='--', marker='s', MS=10)
    # print(mySD1.X.shape ==mySD1.Y.shape,mySD1.Y.shape)
    mySD1.plot()
    mySD1.color='g'
    mySD1.MS=4
    mySD1.marker='<'

    mySD1.plot( scaleY='linear', toAutoscale=1, title='abcf',legend='best', toSave=1, toShow=1 )
    
    mySD2 =sampleDict(X=x1,Y=y1,xlabel= 'x1', ylabel='y1')
    allData=[mySD1,mySD2]
    uPlot(*allData, toAutoscale=1,toShow=1,toSave=0)
    uPlot(*[mySD1,mySD1], toAutoscale=1, legend='lower left')

    Y3= np.sin(10*myX)
    Y4= np.log(10*myX+1)
    Y5= -0.5* myX**2 +6*myX - 8
    # print(Y5)

    sD3=sampleDict(X=myX,Y=Y3, label='sin(10X)')
    sD4=sampleDict(X=myX,Y=Y4, label='log(10.X+1)')
    sD5=sampleDict(X=myX,Y=Y5, label='-0.5x_pow_22+6X-8')

    DTA=list([sD3,sD4,sD5])


    uPlot(*DTA, toShareXaxis=0, toShow=1,toSave=0)
    uPlot(*DTA, toShareXaxis=1, toAutoscale=0, toShow=1,toSave=0, legend='lower left')
    