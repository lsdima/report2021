#encoding:utf-8
from __future__ import print_function
import os
import numpy as np
# from scipy import special
import matplotlib.pyplot as plt
import scipy.optimize
from scipy.optimize import curve_fit

def monoExp(x, m, t, b): return m * np.exp(-t * x) + b #single exponential decay
def fitExp(arr,showPlot=0):
	ys=np.asarray(arr)
	xs=np.arange(len(arr))
	# perform the fit
	# iGuess = (2000, .1, 50) # start with values near those we expect
	iGuess = (200, .1, 50)
	try:
		params, cv = curve_fit(monoExp, xs, ys, iGuess)
	except Exception as e:
		raise
	else:
		# params, cv = scipy.optimize.curve_fit(monoExp, xs, ys, iGuess)
		m, t, b = params
		# sampleRate = 20_000 # Hz
		# tauSec = (1 / t) / sampleRate
		#
		# determine quality of the fit
		ys_f=monoExp(xs, m, t, b)
		# ys_f=np.polyfit(np.log(xs), ys, 1)
		squaredDiffs = np.square(ys - ys_f)
		squaredDiffsFromMean = np.square(ys - np.mean(ys))
		rSquared = 1 - np.sum(squaredDiffs) / np.sum(squaredDiffsFromMean)
		#
		if showPlot:
			print(f"Y = {m} * e^(-{t} * x) + {b}")
			print(f"R² = {rSquared}")
			# print(f"R_L2 = {RL2}")
			# print(f"Tau = {tauSec * 1e6} µs")
			# plot the results
			plt.plot(xs, ys, '.', label="data")
			# plt.plot(xs, monoExp(xs, m, t, b), '--', label="fitted")
			plt.plot(xs, ys_f, '--', label="fitted")
			plt.title("Fitted Exponential Curve")
			# plt.yscale('log') ####!!!!
			plt.grid(1)
			plt.show()
		return ys_f,rSquared #,RL2


def Gaussian (x, Amp, x0, sigm):
    return Amp*np.exp(-np.power(x-x0,2)/(2*np.power(sigm,2)))
#    
def GauFit(X:np.ndarray,Y:np.ndarray):
	assert (len(X) == len(Y)) and (Y.size == np.prod(Y.shape)),'1) {xData.shape != yData.shape} OR {they are both not the 1d-arrays}!'
	sumY=np.sum(Y)
	Mean=np.sum(X*Y)/sumY #The weighted arithmetic mean:
	Sigma = np.sqrt(np.sum(Y*(X-Mean)**2)/sumY ) #np.sum(Y))
	opt_pars, cov = curve_fit(Gaussian, X, Y, p0=[max(Y),Mean,Sigma])
	st_devs=np.sqrt(np.diag(cov))
	FWHM=Sigma*np.sqrt(8*np.log(2.0))

	parsAndCovs={'oSigma':opt_pars[2], 'oAmp':opt_pars[0],'yMax':opt_pars[0],'Mean':opt_pars[1], 'xMax':opt_pars[1],'cov':cov, 'covariance':cov, 'st_devs':st_devs, 'pErr':st_devs,'FWHM':FWHM,'Y_fit':Gaussian(X,*opt_pars)}
	return parsAndCovs


if __name__ == '__main__':
	xdata = np.asarray([ -10.0, -9.0, -8.0, -7.0, -6.0, -5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])
	xdata+=10.
	ydata = np.asarray([1.2, 4.2, 6.7, 8.3, 10.6, 11.7, 13.5, 14.5, 15.7, 16.1, 16.6, 16.0, 15.4, 14.4, 14.2, 12.7, 10.3, 8.6, 6.1, 3.9, 2.1])
	# ydata/=ydata.max()

	# plt.plot(xdata, ydata, 'o')
	parsAndCovs=GauFit(xdata,ydata)
	print(parsAndCovs.keys())
	
	print(f" oSigma = { parsAndCovs['oSigma']:0.4f}")
	
	print(f" xMax = { parsAndCovs['xMax']:0.4f}")
	print(f" yMax = { parsAndCovs['yMax']:0.4f}")
	# print(f" oAmp = { parsAndCovs['oSigma']:0.4f}")
	print()
	print(f"covariance:\n {parsAndCovs['cov']}")
	print()

	print(f"pErr= { parsAndCovs['pErr']} ")
	print()
	print()
	print(f"FWHM = {parsAndCovs['FWHM']:0.4f}")
	print(f"FWHM/2 = {parsAndCovs['FWHM']/2:0.4f}")
	y_fit=parsAndCovs['Y_fit']
	nY_fit=y_fit/parsAndCovs['oAmp']
	

	plt.plot(xdata, ydata, 'b*:', label='data')
	plt.plot(xdata, y_fit, 'r+--',markersize=12, label='fit')
	# plt.plot(xdata,nY_fit, 'mo-',markersize=12,label='normalize')
	plt.legend()
	plt.xlabel('x')
	plt.ylabel('y')
	# plt.xtics=np.linspace()

	plt.grid(1,which ='both' )
	plt.show()



	