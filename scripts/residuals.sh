#!/bin/bash
set -e

# mkdir -p lastLogs
# mv -f Residuals.png lastLogs 2>/dev/null
v=0

if (("$#" != 0)) ; then
	var=$1

	if [ -n "$var" ] && [ "$var" -eq "$var" ] 2>/dev/null; then #check wether var is number
  		# echo -e "Using last $var lines"
  		v=$var
	else
  		echo -e "ERROR! arg. must be an integer"
  		exit 1
	fi
fi

grep "R_rho  norm " solver.log | awk '{print $NF}' > R_rho.txt
grep "R_rhoU norm " solver.log | awk '{print $NF}' > R_rhoU.txt
grep "R_rhoE norm " solver.log | awk '{print $NF}' > R_rhoE.txt
grep "max_Y_L1_norm" solver.log| awk '{print $NF}' > R_rhoY.txt
grep "R_rho_max_rel " solver.log | awk '{print $NF}' > R_rho_rel.txt
grep "R_rhoE_max_rel " solver.log | awk '{print $NF}' > R_rhoE_rel.txt
grep "R_rhoU_max_rel " solver.log | awk '{print $NF}' > R_rhoU_rel.txt
grep "n_cell_rel_error " solver.log | awk '{print $NF}' > n_rel.txt

Xmax=$(cat "R_rho.txt"| wc -l)
Xmin=0
if (("$v" != 0)) ; then
	Xmin=$(($Xmax-$v))
fi

# echo -e "Xmax=$Xmax"
# echo -e "Xmin=$Xmin"

cp -f ../scripts/residuals.gplt residuals.gplt

sed -i "s/ARG1/$Xmin/g" residuals.gplt
sed -i "s/ARG2/$Xmax/g" residuals.gplt

gnuplot residuals.gplt

rm residuals.gplt

# mv -f R_rho* lastLogs 2>/dev/null
# mv -f n_rel* lastLogs 2>/dev/null
# mv -f *.log lastLogs 2>/dev/null
# mv *.png lastLogs
