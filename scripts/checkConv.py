#encoding:utf-8
import os
import numpy as np
# import scipy.optimize
from scipy import stats
import matplotlib.pyplot as plt


def linearFit(X:np.ndarray,Y:np.ndarray):
	
	try:
		res = stats.linregress(X, Y)
	except Exception as e:
		raise
	else:
		R_squared=res.rvalue**2
		# print(f"R-squared: {R_squared:.6f}")
		# coeff-nts for a*x+b
		b=res.intercept
		a=res.slope
		return a,b,R_squared
def log10LinearFit(X:np.ndarray, Y:np.ndarray):
	dSize=len(Y)
	X=np.arange(dSize)
	if X.size!=Y.size:
		print('ERROR in log10LinearFit:\n  X.size != Y.size')
		raise
	if X.size==0:
		print('ERROR in log10LinearFit:\n  X.size == 0')
		raise
	try:
		a,b,R_2= linearFit(X,np.log10(Y))
	except Exception as e:
		print(e)
		raise
	else:
		return a,b,R_2
"""
def checkTimeStep(arr:np.ndarray,R2min:float, showPlot:bool)->bool:
	ll=arr.size
	lim=ll
	while ll>10:
		lim=ll
		ll//=2
	try:
		a,b,R_2=chekConv(np.arange(lim),arr[-lim:])
	except Exception as e:
		print('\nfit FAILED by Exception:,' e)
		return False
	else:
		print(f"R-squared: {R_2:.6f}")
		if R_2<R2min:
			print(f"R-squared < {R2min}\n fit FAILED!")
			return False
		# logYfit=a*X+b
		if showPlot:
			X=np.arange(lim)
			Y=arr[-lim:]
			logYfit=a*X+b
			# plot the results
			#1
			plt.figure(1)
			plt.subplot(211)
			plt.plot(X, np.log10(Y), 'o')
			plt.plot(X, logYfit, '--')
			plt.title("linearFit log10(Y)")
			# plt.yscale('linear') ####!!!!
			plt.grid(1)
			#2
			plt.subplot(212)
			plt.plot(X, Y, '*')
			plt.plot(X, np.power(10,logYfit), 'r-')
			# plt.title("yscale=log")
			plt.yscale('log') ####!!!!
			plt.grid(1)
			plt.show()
		if a < 0:
			print(f'a = {a:} < 0 \n  fit FAILED ')
			return False
		return True
"""
# def checkConv(fileName:str, lastLines:int, showPlot=0, resLim=1.e-04, tailLim=10, R2min=0.8)->bool:
def checkConv(fileName:str, lastLines:int, showPrint=0, showPlot=0, resLim=1.e-04, tailLim=10, R2min=0.8)->bool:
	def checkTimeStep(arr:np.ndarray)->bool:
		ll=arr.size
		lim=ll
		while ll>tailLim:
			lim=ll
			ll//=2
		try:
			a,b,R_2=log10LinearFit(np.arange(lim),arr[-lim:])
		except Exception as e:
			if showPrint:
				print(f'\tfit FAILED by Exception: {e}')
			return False
		else:
			# print(f"R-squared: {R_2:.6f}")
			# logYfit=a*X+b
			if showPlot:
				X=np.arange(lim)
				Y=arr[-lim:]
				logYfit=a*X+b
				# plot the results
				#1
				plt.figure(1)
				plt.subplot(211)
				plt.plot(X, np.log10(Y), 'o')
				plt.plot(X, logYfit, '--')
				plt.title("linearFit log10(Y), R_2={:.4f}".format(R_2))
				# plt.yscale('linear') ####!!!!
				plt.grid(1)
				#2
				plt.subplot(212)
				plt.plot(X, Y, '*')
				plt.plot(X, np.power(10,logYfit), 'r-')
				# plt.title("yscale=log")
				plt.yscale('log') ####!!!!
				plt.grid(1)
				plt.show()
			if R_2<R2min:
				if showPrint:
					print(f"\tfit FAILED! (R_sqr={R_2:.4f}<{R2min})")
				return False
			if a > 0:
				if showPrint:
					print(f'\tfit FAILED! (a={a:.4f}<0 ')
				return False
			return True
	R_data=np.loadtxt(fileName)[-lastLines:]
	splInds=np.argwhere(np.abs(R_data-1.0)<np.finfo(np.float64).eps ).flatten()
	DATA=np.split(R_data,splInds)
	# print('DATA')
	# print(DATA)
	# print('splInds')
	# print(splInds)
	# # print('lenData={}'.format(len(DATA)))
	# print('len splInds={}'.format(len(splInds)))
	XX=np.asarray([])
	YY=np.asarray([])
	#
	numOfTimeSteps=len(DATA)-1
	if numOfTimeSteps<=0:
		if showPrint:
			print('numOfTimeSteps <=0')
		return False
	nDt=0
	for j in range(numOfTimeSteps):
		arr=DATA[j]
		if checkTimeStep(arr):
			xLast=splInds[j]-1
			yLast=arr[-1]
			XX=np.append(XX,xLast)
			YY=np.append(YY,yLast)
			nDt+=1
	if showPlot:
		plt.figure(2)
		plt.plot(XX, YY, '*', )
		plt.plot(XX,np.asarray([np.mean(YY) for i in XX]),'-')
		plt.title("Last Points")
		plt.yscale('log') ####!!!!
		plt.grid(1)
		plt.show()
	mean=np.mean(YY)
	mean<resLim
	if showPrint:
		print(f'{fileName}:\n MEAN finalRes of {nDt} timeSteps = {mean:.4e}')
	return mean<resLim
	
if __name__ == '__main__':
	print(checkConv('R_rho.txt',500, 1, 0 , 1.0e-04, 10, 0.7))
	print(checkConv('R_rhoU.txt',500, 1, 0 , 1.0e-04,10, 0.7))
	print(checkConv('R_rhoE.txt',100, 1, 1 , 1.0e-04,4, 0.7))


