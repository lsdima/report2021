#!/bin/sh

set -e

blockMesh > blockMesh.log

$1 > solver.log

##cp system/controlDict.2 system/controlDict
##$1 >> solver.log

postProcess -func sample -latestTime > sample.log


scripts/residuals.sh

python scr_03.py --resLim 1.0E-03 --threshold 1.0E-02

