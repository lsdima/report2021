#encoding:utf-8
import argparse
import sys
import os
import numpy as np
from scipy import special

import matplotlib.pyplot as plt

def compare(V:np.ndarray, Vr:np.ndarray)->float:
    # np.linalg.norm is for L2 norm:  np.sqrt(np.sum(Xi**2))
    # V  = np.apply_along_axis(np.linalg.norm, 1, U_result) #vector of | U_result(Xi) |
    # Vr = np.apply_along_axis(np.linalg.norm, 1, U_ref)    #vector of | U_ref(Xi) |
    # cosOhi=np.vdot(V,Vr)/()
    R=np.abs(V-Vr)
    abs_err=np.linalg.norm(R)
    # scale=( np.linalg.norm(Vr) + np.linalg.norm(V) )/2
    scale = max(np.linalg.norm(V) ,np.linalg.norm(Vr),1.0e-08)
    rel_err = abs_err / scale
    return rel_err
    # return abs_err

def residualsLookup(Files:list)->float:
    maxRes=0.
    for f in Files:
        ar=np.loadtxt(f)[-100:]
        if ar.size: maxRes=max(maxRes, ar.mean())
    return maxRes

# def U_y(y:float, gradP:float, Mu:float,h:float)->float:return 1./(2*Mu)*y*(y-h)*gradP
# def U_r(r:float, gradP:float, Mu:float,R:float)->float:
#     return (R*R-r*r)*gradP/(4*Mu)


def Y_ref(x:float, y:float, z:float, t:float, D:float, v:float)->float:
    Q=2.5e-05
    s = np.sqrt(x*x + y*y + z*z)
    alpha = s*s/(4*D)
    beta = v*v/(4*D)
    f = 2*np.sqrt(alpha*beta)
    p = np.sqrt(alpha/t)
    q = np.sqrt(beta*t)
    assert s!=0 , "s==0 !!!"
    return (Q/(4*np.pi*D*s))*np.exp(v*z/(2*D))*( np.exp(f)*special.erfc(p+q)+np.exp(-f)*special.erfc(p-q) )

def myPlot(line:np.ndarray,Y:np.ndarray, Y_r:np.ndarray,lineLabel:str):
    fig, ax = plt.subplots()  
    ax.clear()
    ax.set_xlim(line.min(),line.max())  
    ax.grid(1)
    plt.xlabel(lineLabel)
    plt.plot(line,Y,line,Y_r)
    #plt.show()
    nameFig=lineLabel+'.png'
    plt.savefig(nameFig)

def main_Diff()->float:

    Y_zAxis  =np.loadtxt('./postProcessing/sample/0.16/z_axis_Y_H_Y_H2_rho.xy')
    Y_xAxis  =np.loadtxt('./postProcessing/sample/0.16/x_axis_Y_H_Y_H2_rho.xy')
    Y_xShAxis=np.loadtxt('./postProcessing/sample/0.16/x_axis_shifted_Y_H_Y_H2_rho.xy')
    # t_end = 0.06
    t_end = 0.16
    # t_end = 0.1
    D = 5.25e-02
    # D=0.2245

    v = 10.0
    y_r=0.02

    X=Y_xAxis[:,0]
    Xsh=Y_xShAxis[:,0]
    Z=Y_zAxis[:,0]
    # print (X)
    # print('Xsh:')
    # print(Xsh)
    # print('Z')
    # print (Z)

    # print(X)
    # print(Xsh)
    # print(Z)

    Y_H_X=Y_xAxis[:,1]
    Y_H_Xsh=Y_xShAxis[:,1]
    Y_H_Z=Y_zAxis[:,1]

    # def Y_ref(x, y, z, t, D, v):
    
    ref_Y_H_X = np.array([ Y_ref( x, y_r ,0, t_end, D, v ) for x in X ])
    ref_Y_H_Xsh=np.array([ Y_ref( xs, y_r ,0.15,t_end, D, v ) for xs in Xsh ])
    ref_Y_H_Z = np.array([ Y_ref( 0, y_r ,z, t_end, D, v ) for z in Z ])
    # print(ref_Y_H_Z)
    # print(Y_H_Z)

    myPlot(X,Y_H_X,ref_Y_H_X,'X')
    myPlot(Xsh,Y_H_Xsh,ref_Y_H_Xsh,'Xsh'  )
    myPlot(Z,Y_H_Z,ref_Y_H_Z,'Z')

    cX  =compare(Y_H_X,  ref_Y_H_X)
    cXsh=compare(Y_H_Xsh,ref_Y_H_Xsh)
    cZ=compare(Y_H_Z,ref_Y_H_Z)
    print('compare(Y_H_X,ref_Y_H_X)={}'.format(cX))
    print('compare(Y_H_Xsh,ref_Y_H_Xsh)={}'.format(cXsh))
    print('compare(Y_H_Z,ref_Y_H_Z)={}'.format(cZ))

    return max(cX,cXsh,cZ)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compute ')
    # parser.add_argument('file', nargs=2, help='Data file to compare')
    parser.add_argument('--resLim', type=float, default=5.0e-04, help='max relative Residual threshold. Default = 1e-03')
    parser.add_argument('--threshold', type=float, default=2.0e-03, help='Relative error threshold. Default = 1e-02')
    args = parser.parse_args()
    
    errLim=args.threshold
    maxResThr= args.resLim

    lsFiles=[ i for i in os.listdir("./") if ( i.endswith('rel.txt') and i.startswith('R') ) ]


    maxResidual= residualsLookup(lsFiles)

    sol_rel_dif=main_Diff()
    print("mainDiff = {}".format(sol_rel_dif))
    print("maxResidual = {}".format(maxResidual))

    if maxResidual > maxResThr :
        print('FAIL: SOLUTION SEEMS TO HAVE NOT CONVERGED! Resulting maximal residual ({0:.4e}) > resLim ({1:.4e})'.format(maxResidual, maxResThr) )
        sys.exit(1)

    elif sol_rel_dif > errLim :
        print('FAIL: resulting Diff ({0:.4e}) > threshold ({1:.4e})'.format(sol_rel_dif, errLim) )
        sys.exit(1)
    else:
        print('PASS')
