#!/bin/sh

set -e

grep "R_rho  norm " solver.log | awk '{print $NF}' > R_rho.txt
grep "R_rhoU norm " solver.log | awk '{print $NF}' > R_rhoU.txt
grep "R_rhoE norm " solver.log | awk '{print $NF}' > R_rhoE.txt
grep "max_Y_L1_norm" solver.log| awk '{print $NF}' > R_rhoY.txt
grep "R_rho_max_rel " solver.log | awk '{print $NF}' > R_rho_rel.txt
grep "R_rhoE_max_rel " solver.log | awk '{print $NF}' > R_rhoE_rel.txt
grep "R_rhoU_max_rel " solver.log | awk '{print $NF}' > R_rhoU_rel.txt
grep "n_cell_rel_error " solver.log | awk '{print $NF}' > n_rel.txt

gnuplot scripts/residuals.gplt