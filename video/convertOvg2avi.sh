#!/bin/sh
set -e

for i in *.ogv; do
	#ffmpeg -i "$i" -preset veryslow -crf 22  -c:a aac -b:a 128k -strict -2 "$i.mp4"
	ffmpeg -i "$i" -vcodec copy -acodec copy "$i.avi"
	# ${i::(-3)}" means  w/o last 3 characters
done
