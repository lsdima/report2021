#!/bin/sh
set -e

rm -rf processor*

blockMesh > blockMesh.log
checkMesh >> blockMesh.log

decomposePar -force > decompose.log

#mapFields ../18a_2d_forward_facing_step/ -consistent -parallelTarget

mpirun -np 4 $1 -parallel 2>&1 > solver.log


reconstructPar > reconstruct.log
rm -rf processor*

postProcess -func "grad(T)" > ppFunc.log
postProcess -func "grad(p)" >> ppFunc.log
foamToVTK > ftvtk.log
postProcess -func sample  > sample.log
#postProcess -func sample -latestTime >>sample.log

bash ../scripts/residuals.sh 1000

python scr_18.py --resLim 5.1E-04 --machStemWidth 6 --tangentDiscontWidth 8 >Scr.log
