#encoding:utf-8
import argparse
import sys,os
import numpy as np
# from scipy import special
paath = os.path.abspath('../scripts/')
sys.path.insert(1, paath)
from checkConv import checkConv
from universalPlot import sampleDict as SD
from universalPlot import uPlot as uPlot
from fit import GauFit


#
def cellSize(pntCoords:np.ndarray, xLims:list, N=14)->float:
    assert pntCoords.size == np.prod(pntCoords.shape), 'pntCoords is not a 1-D np.ndarray !'
    h=-1.0
    if not ( xLims is None):
        assert (len(xLims)==2 and xLims[0]<xLims[1]), 'Invalid xLims !'
        pntCoords=pntCoords[(pntCoords> xLims[0]) & (pntCoords<xLims[1])]# fine extraction techknique it is!
        h=(pntCoords[-1] - pntCoords[0])/(pntCoords.size-1)
    elif (xLims is None) and N:
        assert N > 10, 'N is 2small   ! '
        assert N < pntCoords.size-2, 'N is 2big! '
        h=(pntCoords[-1] - pntCoords[-N])/(N-1)
    else:
        h=(pntCoords[-1] - pntCoords[0])/(pntCoords.size-1)
    print(f'h={h:0.4f}')
    return h
#
def largestKupol(XY:np.ndarray, **args)->(int,int,int,np.ndarray):
    assert np.shape(XY)[1]==2,"2D nparray expected at the input!"
    assert np.abs(XY[:,1].min())<XY[:,1].max(),"|min(XY)|>max(XY)! Invert your XY Data!"
    XY[:,1]=XY[:,1].clip(min=0)
    crsnr=0.1
    if 'coarsener' in args.keys():
        crsnr=args['coarsener']
    assert type(crsnr)==float, 'coarsener is not a number!'
    assert crsnr>float(0), 'coarsener <= 0 '

    eps=crsnr*XY[:,1].mean()
    iM=np.argmax(XY[:,1])
    iL=iM
    iR=iM
    while iL>1 and XY[iL,1]>eps and XY[iL-1,1]<XY[iL,1]:
        iL-=1
    while iR<(len(XY)-1) and XY[iR,1]>eps and XY[iR,1]>XY[iR+1,1]:
        iR+=1
    for j in range(len(XY)):
        if j<=iL or j>=iR:
            XY[j,1]=float(0)

    if 'printResults' in args.keys()  and args['printResults']:
        if 'briefPrint' in args.keys() and args['briefPrint']:
            print(f'iL={iL},iM={iM},iR={iR}')
            print(f'xL={XY[iL,0]},xM={XY[iM,0]}, xR={XY[iR,0]}')
        else:
            print(f'iL={iL},iM={iM},iR={iR}')
            print(f'xL={XY[iL,0]},xM={XY[iM,0]}, xR={XY[iR,0]}')
            print(f'XY=')
            print(XY)
            print(f'---------------------\n')
    return iL,iM, iR, XY
#
def getSamples(**args):
    tX={}
    tY={}
    path_samples= './postProcessing/sample'

    # timeFs=[os.path.join(path_samples,subdir) for subdir in sorted(os.listdir(path_samples))]

    # gap where to search for peak:
    # 1 for x component of grad(T) (for Mach's stem detection )
    # 2 for y component of grad(T) (for tangential discontinuiety detection)
    showGr=0 # wether to save the pictures
    if 'showGr' in args.keys():
        showGr=args['showGr']
    #gap where to catch the discontinuiety
    min1=0.4
    max1=0.8

    min2=0.65
    max2=0.85


    hX=-1.#default invalid value
    hY=-1.#default invalid value
    timeFs=[subdir for subdir in sorted(os.listdir(path_samples))]

    # the divider to divide data into parts  to keep the  last part  to work with:
    iviDer=6#8 #2 # the divider to divide data into parts  to keep the  last part  to work with
    if len(timeFs)//iviDer>2:
        timeFs=timeFs[-(len(timeFs)//iviDer):]
    sampListX=[]
    sampListY=[]
    sd_x =None
    usd_x=None
    sd_y=None
    usd_y=None
    print(f'times=\n{timeFs}')
    for di in timeFs:
        for fi in os.listdir(os.path.join(path_samples,di)):
            if fi.startswith('lineX_g'):
                Fi=os.path.join(path_samples,di,fi)
                if os.path.isfile(Fi):
                    dataX=np.loadtxt(Fi,usecols=[0,1])
                    # if hX<0:
                    #     Xs=dataX[:,0]
                    #     hX=cellSize(Xs,[min1,max1])
                    ###
                    I=np.argwhere((dataX[:,0]>=min1)&(dataX[:,0]<=max1))
                    dataX=dataX[I.min():I.max()]
                    ####
                    # iL,iM,iR,dataX=largestKupol(dataX,coarsener=0.2,printResults=1,briefPrint=1)
                    iL,iM,iR,dataX=largestKupol(dataX,coarsener=0.2)
                    wdth=np.abs(dataX[iR,0]-dataX[iL,0])
                    cellW=wdth/(iR-iL)
                    pos=dataX[iM,0]
                    intWidth=iR-iL
                    # pos=0.5*(dataX[iL,0]+dataX[iR,0])
                    # print(f'pos={pos:0.04f}, wdth={wdth:0.04f}, cellW={cellW:0.4f}')
                    # print(f'width in {iR-iL} cells')
                    # tX[di]=(pos,wdth)
                    tX[di]=(pos,intWidth)

                    ####
                    if showGr:
                        sampListX.append(dataX)
                        sd_x=SD(XY=dataX,color='r',marker='o', label='sampX')
                        sd_x.ylabel='grad(T)_x'
                        sd_x.xlabel='x'
                        # sd_x.xLims=[min1,max1]
                        sd_x.plot( title='X_smp__'+di,legend='best', toSave=1, toShow=0,reset=1 )

            elif fi.startswith('lineY_g'):
                Fj=os.path.join(path_samples,di,fi)
                if os.path.isfile(Fj):
                    dataY=np.loadtxt(Fj,usecols=[0,2])
                    # if hY<0:
                    #     Ys=dataY[:,0]
                    #     hY=cellSize(Ys,[min2,max2])
                    ######
                    J=np.argwhere((dataY[:,0]>=min2)&(dataY[:,0]<=max2))
                    
                    dataY=dataY[J.min():J.max()]

                    # jL,jM,jR,dataY=largestKupol(dataY,coarsener=0.3,printResults=1,briefPrint=1)
                    jL,jM,jR,dataY=largestKupol(dataY,coarsener=0.3)
                    wdth=np.abs(dataY[jR,0]-dataY[jL,0])
                    cellW=wdth/(jR-jL)
                    intWidth=jR-jL
                    pos=dataY[jM,0]
                    # tY[di]=(pos,wdth)
                    tY[di]=(pos,intWidth)

                    # pos=0.5*(dataY[jL,0]+dataY[jR,0])
                    # print(f'pos={pos:0.04f}, wdth={wdth:0.04f}, cellW={cellW:0.4f}')
                    # print(f'width in {jR-jL} cells')
                    if showGr:
                        sampListY.append(dataY)
                        sd_y=SD(XY=dataY,color='g',marker='o',label='sampListY')
                        # sd_y.xlabel= ''
                        sd_y.ylabel='grad(T)_y'
                        sd_y.xlabel='y'
                        # sd_y.xLims=[min2,max2]
                        sd_y.plot( title='Y_smp__'+di,legend='best', toSave=1, toShow=0,reset=1 )
    return tX,tY

if __name__ == '__main__':
    #TODO exampl: python scr_18.py --resLim 5.1E-04 --machStemWidth 6 --tangentDiscontWidth 8

    parser = argparse.ArgumentParser(description='Compute some stuff')
    # parser.add_argument('file', nargs=2, help='Data file to compare')
    parser.add_argument('--resLim', type=float, default=5.0e-04, help='max relative Residual threshold.')
    # parser.add_argument('--threshold', type=float, default=2.0e-03, help='Relative error threshold.')
    parser.add_argument('--machStemWidth', type=int, default=6 ,help='Discontinuiety front width in cell sizes.(almoust regular grid h*h=0.0125*0.0125).')
    parser.add_argument('--tangentDiscontWidth', type=int, default=8, help='Discontinuiety front width in cell sizes.(almoust regular grid h*h=0.0125*0.0125).')
    args = parser.parse_args()

    # checkConv(fileName:str, lastLines:int, showPrint=0, showPlot=0, resLim=1.e-04, tailLim=10, R2min=0.8)->bool:
    # print(checkConv('R_rho.txt',500, 1, 0 , 1.0e-04, 10, 0.7))
    # print(checkConv('R_rhoU.txt',500, 1, 0 , 1.0e-04,10, 0.7))
    # print(checkConv('R_rhoE.txt',100, 1, 1 , 1.0e-04,4, 0.7))
    # print(checkConv('R_rhoU.txt',100, 1, 0 , 1.0e-04,10, 0.7))
    if not checkConv('R_rhoU.txt',100, 1, 0 , args.resLim,10, 0.7):
        print('FAIL: SOLUTION SEEMS TO HAVE NOT CONVERGED! Resulting maximal residual !' )
        sys.exit(1)

    tX,tY=getSamples(showGr=0)
    # print(tX)
    # print("------------------------------------------------")
    # print(tY)
    t=sorted(tX.keys())
    T=np.asarray(t)

    X=np.asarray([tX[i][0] for i in t])
    Xw=np.asarray([tX[i][1] for i in t])

    Y=np.asarray([tY[i][0] for i in t])
    Yw=np.asarray([tY[i][1] for i in t])
    SDtx=SD(X=T,Y=X, xlabel='t',ylabel='position',label='t_vs_position', color='m', marker='s',markersize=12)
    SDty=SD(X=T,Y=Y, xlabel='t',ylabel='position', label='t_vs_position',color='b', marker='o')
    SDtx.plot(toShow=0,toSave='tx')
    SDty.plot(toShow=0,toSave='ty')
    SDtxw=SD(X=T,Y=Xw, xlabel='t',ylabel='wdth', label='t_vs_X_width',color='r', marker='>',markersize=12)
    SDtyw=SD(X=T,Y=Yw, xlabel='t',ylabel='wdth', label='t_vs_Y_width',color='g', marker='*')
    SDtxw.plot(toShow=0,toSave='txw')
    SDtyw.plot(toShow=0,toSave='tyw')
    if Xw[-1]> args.machStemWidth:
        print(f'FAIL: machStemWidth={Xw[-1]} > args.machStemWidth={args.machStemWidth}')
        sys.exit(1)
    if Yw[-1]> args.tangentDiscontWidth:
        print(f'FAIL: tangentDiscontWidth={Yw[-1]} > args.tangentDiscontWidth={args.tangentDiscontWidth}')
        sys.exit(1)

    else:
        print('PASS')

##############################################################################################################################
##############################################################################################################################
##############################################################################################################################

# sampListY.append(np.loadtxt(Fj,usecols=[0,2]))
# print('sampListX')
# print(sampListX)
# print('sampListY')
# print(sampListY)
# print
# return
"""
for i in range(len(sampList)):
    
    SMP=np.loadtxt(sampList[i]) #columns:   x, T, mag(U), p, rho
    X = SMP[:,0]
    T = SMP[:,1]
    U = SMP[:,2]
    P = SMP[:,3]
    rho=SMP[:,4]
    #
    REF=np.loadtxt(refList[i],skiprows=1) #columns:    x,p,rho,u,e,E,c,M,T,h
    #                                                  0 1  2  3 4 5 6 7 8 9
    refX = REF[:,0]
    refT = REF[:,8]
    refU = REF[:,3]
    refP = REF[:,1]
    refRho=REF[:,2]
    assert len(X)==len(refX), "len(X)!=len(refX) !"
    assert compare(X,refX)<= np.finfo(np.float32).eps , "X does not coincide with refX !"

"""
# return max(cX,cXsh,cZ)