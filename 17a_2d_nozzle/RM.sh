#!/bin/bash

foamListTimes -rm
mkdir -p lastLogs

mv -f R_rho* lastLogs 2>/dev/null
mv -f n_rel* lastLogs 2>/dev/null
mv -f *.log lastLogs 2>/dev/null
mv *.png lastLogs
mv -f VTK lastLogs/
