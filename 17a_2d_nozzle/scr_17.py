#encoding:utf-8
import argparse
import sys,os
import numpy as np
# from scipy import special
paath = os.path.abspath('../scripts/')
sys.path.insert(1, paath)
from checkConv import checkConv
from universalPlot import sampleDict as SD
from universalPlot import uPlot as uPlot
# from fit import GauFit

def largestKupol(XY:np.ndarray, **args)->(int,int,int,np.ndarray):
    assert np.shape(XY)[1]==2,"2D nparray expected at the input!"
    assert np.abs(XY[:,1].min())<XY[:,1].max(),"|min(XY)|>max(XY)! Invert your XY Data!"
    XY[:,1]=XY[:,1].clip(min=0)
    crsnr=0.1
    if 'coarsener' in args.keys():
        crsnr=args['coarsener']
    assert type(crsnr)==float, 'coarsener is not a number!'
    assert crsnr>float(0), 'coarsener <= 0 '
    #
    
    eps=crsnr*XY[:,1].mean()
    iM=np.argmax(XY[:,1])
    iL=iM
    iR=iM
    while iL>1 and XY[iL,1]>eps and XY[iL-1,1]<XY[iL,1]:
        iL-=1
    while iR<(len(XY)-1) and XY[iR,1]>eps and XY[iR,1]>XY[iR+1,1]:
        iR+=1
    for j in range(len(XY)):
        if j<=iL or j>=iR:
            XY[j,1]=float(0)
    #
    if 'printResults' in args.keys()  and args['printResults']:
        if 'briefPrint' in args.keys() and args['briefPrint']:
            print(f'iL={iL},iM={iM},iR={iR}')
            print(f'xL={XY[iL,0]},xM={XY[iM,0]}, xR={XY[iR,0]}')
        else:
            print(f'iL={iL},iM={iM},iR={iR}')
            print(f'xL={XY[iL,0]},xM={XY[iM,0]}, xR={XY[iR,0]}')
            print(f'XY=')
            print(XY)
            print(f'---------------------\n')
    return iL,iM, iR, XY
#
def getSamples(**args):
    resultDict={}
    path_samples= './postProcessing/sampleByCells'
    # path_samples= './postProcessing/sampleByCellPointFace'
    if 'sample_pth' in args.keys():
        path_samples=args['sample_pth']

    crsnr=0.2
    if 'coarsener' in args.keys() and float(args['coarsener'])>float(0.):
            crsnr=float(args['coarsener'])
    #            
    #gap where to catch the discontinuiety
    min1=-0.2
    max1=1.0
    lms=[min1,max1]
    if ('gap' in args.keys()) and isinstance(args['gap'],list) and len(args['gap'])==2:
        lms=args['gap']
        assert lms[0]<lms[1], 'wrong gaps !'
        min1=lms[0]
        max1=lms[1]

    timeFs=[subdir for subdir in sorted(os.listdir(path_samples))]

    # the divider to divide data into parts  to keep the  last part  to work with:
    iviDer=2
    if 'divider' in args.keys() and int(args['divider'])>=2:    
        iviDer=int(args['divider'])
    if len(timeFs)//iviDer>2:
        timeFs=timeFs[-(len(timeFs)//iviDer):]

    fNamePtrn='lineX_for_width'
    if 'startswith' in args.keys():
        fNamePtrn=args['startswith']

    sampListX=[]
    sd_x =None
    # sampListY=[]
    # sd_y=None

    print(f'times=\n{timeFs}')
    for di in timeFs:
        for fi in os.listdir(os.path.join(path_samples,di)):
            # if fi.startswith('lineX_g'):
            if fi.startswith(fNamePtrn):
                Fi=os.path.join(path_samples,di,fi)
                if os.path.isfile(Fi):
                    dataX=np.loadtxt(Fi,usecols=[0,1])
                    ###
                    I=np.argwhere((dataX[:,0]>=min1)&(dataX[:,0]<=max1))
                    dataX=dataX[I.min():I.max()]
                    ####
                    # iL,iM,iR,dataX=largestKupol(dataX,coarsener=crsnr,printResults=1,briefPrint=1)
                    iL,iM,iR,dataX=largestKupol(dataX,coarsener=crsnr)
                    wdth=np.abs(dataX[iR,0]-dataX[iL,0])
                    cellW=wdth/(iR-iL)
                    pos=dataX[iM,0]
                    intWidth=iR-iL
                    # pos=0.5*(dataX[iL,0]+dataX[iR,0])
                    # print(f'pos={pos:0.04f}, wdth={wdth:0.04f}, cellW={cellW:0.4f}')
                    # print(f'width in {iR-iL} cells')
                    # resultDict[di]=(pos,wdth)
                    resultDict[di]=(pos,intWidth)
                    ####
                    if 'showGr' in args.keys() and args['showGr']:
                        sampListX.append(dataX)
                        sd_x=SD(XY=dataX,color='r',marker='o', label=fNamePtrn)
                        sd_x.ylabel='grad(p)'
                        sd_x.xlabel='x'
                        sd_x.xLims=[0.05,0.2]
                        sd_x.plot( title=fNamePtrn+di,legend='best', toSave=1, toShow=0,reset=1 )
            """
            elif fi.startswith('lineY_g'):
                Fj=os.path.join(path_samples,di,fi)
                if os.path.isfile(Fj):
                    dataY=np.loadtxt(Fj,usecols=[0,2])
                    # if hY<0:
                    #     Ys=dataY[:,0]
                    #     hY=cellSize(Ys,[min2,max2])
                    ######
                    J=np.argwhere((dataY[:,0]>=min2)&(dataY[:,0]<=max2))
                    
                    dataY=dataY[J.min():J.max()]

                    # jL,jM,jR,dataY=largestKupol(dataY,coarsener=0.3,printResults=1,briefPrint=1)
                    jL,jM,jR,dataY=largestKupol(dataY,coarsener=0.3)
                    wdth=np.abs(dataY[jR,0]-dataY[jL,0])
                    cellW=wdth/(jR-jL)
                    intWidth=jR-jL
                    pos=dataY[jM,0]
                    # tY[di]=(pos,wdth)
                    tY[di]=(pos,intWidth)

                    # pos=0.5*(dataY[jL,0]+dataY[jR,0])
                    # print(f'pos={pos:0.04f}, wdth={wdth:0.04f}, cellW={cellW:0.4f}')
                    # print(f'width in {jR-jL} cells')
                    if 'showGr' in args.keys() and args['showGr']:
                        sampListY.append(dataY)
                        sd_y=SD(XY=dataY,color='g',marker='o',label='sampListY')
                        # sd_y.xlabel= ''
                        sd_y.ylabel='grad(T)_y'
                        sd_y.xlabel='y'
                        # sd_y.xLims=[min2,max2]
                        sd_y.plot( title='Y_smp__'+di,legend='best', toSave=1, toShow=0,reset=1 )
            """                        
    return resultDict

if __name__ == '__main__':
    """
    #TODO exampl:
    # python scr_17.py --resLim 5.1E-04 --machStemWidth 9 --relaxTime 0.087 >Scr.log
    """
    parser = argparse.ArgumentParser(description='Compute some stuff')
    parser.add_argument('--resLim', type=float, default=5.0e-04, help='Max relative Residual threshold.')
    parser.add_argument('--machStemWidth', type=int, default=9 ,help='Discontinuiety front width in cell sizes.(almoust regular grid h*h=0.0125*0.0125).')
    parser.add_argument('--relaxTime', type=float, default=0.9 ,help='Time to establish the 1st Mach disks position.')#0.87
    args = parser.parse_args()

    # checkConv(fileName:str, lastLines:int, showPrint=0, showPlot=0, resLim=1.e-04, tailLim=10, R2min=0.8)->bool:
    # print(checkConv('R_rho.txt',500, 1, 0 , 1.0e-04, 10, 0.7))
    # print(checkConv('R_rhoU.txt',500, 1, 0 , 1.0e-04,10, 0.7))
    # print(checkConv('R_rhoE.txt',100, 1, 1 , 1.0e-04,4, 0.7))
    # print(checkConv('R_rhoU.txt',100, 1, 0 , 1.0e-04,10, 0.7))
    if not checkConv('R_rhoU.txt',2000, 1, 0 , args.resLim,10, 0.7):
        print('FAIL: SOLUTION SEEMS TO HAVE NOT CONVERGED! Resulting maximal residual !' )
        sys.exit(1)
    #
    tX=getSamples(showGr=1 )
    #
    t=sorted(tX.keys())
    #
    # print(tX)
    # print("------------------------------------------------")
    # # print(tY)
    # print()
    # print(t)
    # print("------------------------------------------------")
    # print(t[::-1])
    # print()
    #
    T=np.asarray(t)

    X=np.asarray([tX[i][0] for i in t])
    Xw=np.asarray([tX[i][1] for i in t])
    SDtx=SD(X=T,Y=X, xlabel='t',ylabel='position',label='t_vs_position', color='m', marker='s',markersize=12)
    SDtx.plot(toShow=0,toSave='tx')
    SDtxw=SD(X=T,Y=Xw, xlabel='t',ylabel='wdth', label='t_vs_X_width',color='r', marker='>',markersize=12)
    SDtxw.plot(toShow=0,toSave='txw')
    if Xw[-1]> args.machStemWidth:
        print(f'FAIL: machStemWidth={Xw[-1]} > args.machStemWidth={args.machStemWidth}')
        sys.exit(1)
    #
    rt=t[::-1] #reversed t
    cond=1
    k=1
    while cond:
        if np.abs(tX[rt[k]][0] - tX[rt[0]][0])>np.finfo(np.float64).eps:
            print(f'k={k}, time={rt[k]}')
            if float(rt[k])>args.relaxTime:
                print(f'FAIL: establishing time={rt[k]} > limit={args.relaxTime}')
                sys.exit(1)
            cond=0
        k+=1
    # else:
    print('PASS')

