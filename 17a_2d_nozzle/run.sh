#!/bin/sh
set -e
#
rm -rf processor*

blockMesh > blockMesh.log
checkMesh >> blockMesh.log

decomposePar -force > decompose.log

#mapFields ../18a_2d_forward_facing_step/ -consistent -parallelTarget

mpirun -np 4 $1 -parallel 2>&1 > solver.log


reconstructPar > reconstruct.log
rm -rf processor*

# postProcess -func "grad(T)" > ppFunc.log
postProcess -func "grad(p)" >> ppFunc.log
foamToVTK > ftvtk.log

postProcess -func sampleByCells > sample.log
postProcess -func sampleByCellPointFace >> sample.log

bash ../scripts/residuals.sh 2000

python scr_17.py --resLim 1.0E-03 --machStemWidth 9 --relaxTime 0.087 >Scr.log
