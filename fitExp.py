#encoding:utf-8
import os
import numpy as np
# from scipy import special

import matplotlib.pyplot as plt

from __future__ import print_function
import scipy.optimize
from scipy.optimize import curve_fit


def compareL2(V:np.ndarray, Vr:np.ndarray)->float:
	R=np.abs(V-Vr)
	abs_err=np.linalg.norm(R)
	scale = max(np.linalg.norm(V) ,np.linalg.norm(Vr),1.0e-16)
	rel_err = abs_err / scale
	return rel_err


def monoExp(x, m, t, b): return m * np.exp(-t * x) + b #single exponential decay
def fitExp(arr,showPlot=0):
	ys=np.asarray(arr)
	xs=np.arange(len(arr))
	# perform the fit
	# iGuess = (2000, .1, 50) # start with values near those we expect
	iGuess = (200, .1, 50)
	try:
		params, cv = curve_fit(monoExp, xs, ys, iGuess)
	except Exception as e:
		raise
	else:
		# params, cv = scipy.optimize.curve_fit(monoExp, xs, ys, iGuess)
		m, t, b = params
		# sampleRate = 20_000 # Hz
		# tauSec = (1 / t) / sampleRate
		#
		# determine quality of the fit
		ys_f=monoExp(xs, m, t, b)
		# ys_f=np.polyfit(np.log(xs), ys, 1)
		squaredDiffs = np.square(ys - ys_f)
		squaredDiffsFromMean = np.square(ys - np.mean(ys))
		rSquared = 1 - np.sum(squaredDiffs) / np.sum(squaredDiffsFromMean)
		# RL2=compareL2(ys,ys_f)
		#
		if showPlot:
			print(f"Y = {m} * e^(-{t} * x) + {b}")
			print(f"R² = {rSquared}")
			# print(f"R_L2 = {RL2}")
			# print(f"Tau = {tauSec * 1e6} µs")
			# plot the results
			plt.plot(xs, ys, '.', label="data")
			# plt.plot(xs, monoExp(xs, m, t, b), '--', label="fitted")
			plt.plot(xs, ys_f, '--', label="fitted")
			plt.title("Fitted Exponential Curve")
			# plt.yscale('log') ####!!!!
			plt.grid(1)
			plt.show()
		return ys_f,rSquared #,RL2



xdata = [ -10.0, -9.0, -8.0, -7.0, -6.0, -5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
ydata = [1.2, 4.2, 6.7, 8.3, 10.6, 11.7, 13.5, 14.5, 15.7, 16.1, 16.6, 16.0, 15.4, 14.4, 14.2, 12.7, 10.3, 8.6, 6.1, 3.9, 2.1]

# Recast xdata and ydata into numpy arrays so we can use their handy features
xdata = np.asarray(xdata)
ydata = np.asarray(ydata)
plt.plot(xdata, ydata, 'o')

# Define the Gaussian function
def Gaussian(x, A, B):
	y = A*np.exp(-1*B*x**2)
	return y
parameters, covariance = curve_fit(Gaussian, xdata, ydata)

fit_A = parameters[0]
fit_B = parameters[1]

fit_y = Gaussian(xdata, fit_A, fit_B)
plt.plot(xdata, ydata, 'o', label='data')
plt.plot(xdata, fit_y, '-', label='fit')
plt.legend()



def residualsLookup(fileName:str, lastLines:int, showPlot=0)->float:
	R_data=np.loadtxt(fileName)[-lastLines:]
	splInds=np.argwhere(np.abs(R_data-1.0)<np.finfo(np.float64).eps ).flatten()
	DATA=np.split(R_data,splInds)
	# print('DATA')
	# print(DATA)
	# print('splInds')
	# print(splInds)
	# # print('lenData={}'.format(len(DATA)))
	# print('len splInds={}'.format(len(splInds)))
	XX=np.asarray([])
	YY=np.asarray([])
	#
	for j in range(len(DATA)-1):
		if showPlot:print('j={}'.format(j))
		arr=DATA[j]
		# yLast=arr[-1]
		xLast=splInds[j]-1
		yLast=arr[-1]
		try:
			y_s,rSquared=fitExp(arr[-len(arr)//2:],showPlot)
		except Exception as e:
			print(e)
			print('ERROR EXCP>')
			pass
		else:
			if rSquared>0.9 and y_s[-1]>0:
				yLast=y_s[-1]
				# print('yLast= y_s[-1]={}'.format(y_s[-1]))
				# print('GOOD!!+++!!!!\n')
				# print("{}".format(np.abs(yLast -arr[-1])<np.finfo(np.float64).eps))
			else:
				# print('BAD FIT quality!\n')
				# print("{}".format(np.abs(yLast -arr[-1])<np.finfo(np.float64).eps))
				# print('yLast=arr[-1]={}'.format(arr[-1]))
				pass
		
		# print('yLast  = {}'.format(yLast))
		# print('arr[-1]= {}'.format(arr[-1]))
		# print('DIFF={:.7e}'.format(np.abs(arr[-1]-yLast)))
		# print("yLast=arr[-1] ? {}".format(np.abs(yLast -arr[-1])<np.finfo(np.float64).eps))
		# print("yLast=arr[-1] ? {}".format(np.abs(yLast -arr[-1])<np.finfo(np.float64).eps))
		if np.abs(yLast -arr[-1])<np.finfo(np.float64).eps:
			print("FAILED")
		XX=np.append(XX,xLast)
		YY=np.append(YY,yLast)

	if showPlot:
		plt.plot(XX, YY, '*', )
		plt.plot(XX,np.asarray([np.mean(YY) for i in XX]),'-')
		plt.title("Last Points")
		plt.yscale('log') ####!!!!
		plt.grid(1)
		plt.show()
	return np.mean(YY)
	
if __name__ == '__main__':
	N0=500
	rres=residualsLookup('R_rho.txt',N0,1)
	print('\n MEAN of last {} Points = {}'.format(N0,rres))
 # History fight aside, I want to say there are two styles of engineering I absolutely adore.
 # One of them is “I am fuelled solely by spite and rage. My creation will kill God or I will die trying” 