#encoding:utf-8
import argparse
import sys
import numpy as np


def mDiff(V1,V2):
    cosPhi=np.vdot(V1,V2)/( np.linalg.norm(V1)*np.linalg.norm(V2) )
    R=V1-V2
    R=np.abs(R)
    absErr=np.linalg.norm(R)
    scale=max(np.linalg.norm(V1),np.linalg.norm(V2))
    relErr=absErr/scale
    print('cosPhi={0}, relErr={1}'.format(cosPhi,relErr))
    return (cosPhi,relErr)


def compare(V:np.ndarray, Vr:np.ndarray)->float:
    # np.linalg.norm is for L2 norm:  np.sqrt(np.sum(Xi**2))
    # V  = np.apply_along_axis(np.linalg.norm, 1, U_result) #vector of | U_result(Xi) |
    # Vr = np.apply_along_axis(np.linalg.norm, 1, U_ref)    #vector of | U_ref(Xi) |
    cosOhi=np.vdot(V,Vr)/()
    R=np.abs(V-Vr)
    abs_err=np.linalg.norm(R)
    # scale=( np.linalg.norm(Vr) + np.linalg.norm(V) )/2
    scale = np.max([np.linalg.norm(V) ,np.linalg.norm(Vr),1.0e-08])
    rel_err = abs_err /scale
    return rel_err


def compareDataTables(file1:str, file2:str, errLim2=np.finfo(float).eps)->float:
    result_table = np.loadtxt(file1)
    ref_table    = np.loadtxt(file2)
    # By default, genfromtxt assumes delimiter=None, meaning that the line is
    # split along white spaces (including tabs) and that consecutive white
    # spaces are considered as a single white space.

    assert result_table.shape[0] == ref_table.shape[0], "Input sample data tables have different lengths!"
    # assert result_table.shape[1]== 4, "result data table column number != 4"
    X=result_table[:,[0]]
    X_ref=ref_table[:,[0]]

    # floatEps=(np.finfo(float).eps)
    # x_rel_err=np.linalg.norm(X-X_ref)/(0.5*(np.linalg.norm(X)+np.linalg.norm(X_ref)))
    # assert x_rel_err < floatEps , "sample points spatial coordinates do not match!"

    x_err=np.linalg.norm(X-X_ref)
    errLine='sample points spatial coordinates do not match! ( x_err = {} )'.format(x_err)
    assert x_err < errLim2 , errLine
    

    valRelDiffList=[]  

    for i in range(1,min(result_table.shape[1],ref_table.shape[1])):
        valRelDiffList.append(compare(result_table[:,i],ref_table[:,i]))
        
    # print("\n\terros:\t",valRelDiffList,"\n")
    return  np.max(valRelDiffList)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compute relative difference between two data files')
    parser.add_argument('file', nargs=2, help='Data file to compare')
    parser.add_argument('-t', '--threshold', type=float, default=1e-06, help='Relative error threshold. Default = 1e-06')
    args = parser.parse_args()
    finallyErr = compareDataTables(args.file[0], args.file[1], args.threshold)
    errLim=args.threshold
    if finallyErr > errLim :
        print('FAIL: Resulting error ({0:.4e}) > threshold ({1:.4e})'.format(finallyErr, errLim) )
        sys.exit(1)
    else:
        print('PASS')