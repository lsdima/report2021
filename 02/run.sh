#!/bin/sh
#
set -e

blockMesh > blockMesh.log

echo "Running $1 in $pwd"

$1 > solver.log

#scripts/residuals.sh

postProcess -func sample -time 0.1 > sample.log

PPFolder="./postProcessing/sample/0.1/"
REFolder="./ref/sample/0.1/"

#python scripts/compare01.py --threshold 2.0E-02 $PPFolder/line_U.xy $REFolder/ref_line_U.xy > compare.log
#python scripts/compare01.py --threshold 2.0E-02 $PPFolder/line_T_p_rho.xy $REFolder/ref_line_T_p_rho.xy >> compare.log

python scr_02.py --threshold 2.0E-02 $PPFolder/line_U.xy $REFolder/ref_line_U.xy > compare.log
python scr_02.py --threshold 2.0E-02 $PPFolder/line_T_p_rho.xy $REFolder/ref_line_T_p_rho.xy >> compare.log
