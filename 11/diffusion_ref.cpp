#include "fvCFD.H"
#include "fvMesh.H"

#include <cmath>

/* solution according to presentation transient times v14 7-11-2016 */

int main(int argc, char *argv[]) {
    timeSelector::addOptions();
    /* Set root case */
    Foam::argList args(argc, argv);
    if (!args.checkRootCase()) {
        Foam::FatalError.exit();
    }
    /* Create time */
    Foam::Info<< "Create time\n" << Foam::endl;
    Foam::Time runTime(Foam::Time::controlDictName, args);
    instantList timeDirs = timeSelector::select0(runTime, args);
    /* Create mesh */
    Foam::Info  << "Create mesh for time = "  << runTime.timeName() << Foam::nl << Foam::endl;
    Foam::fvMesh mesh ( Foam::IOobject ( Foam::fvMesh::defaultRegion, runTime.timeName(), runTime, Foam::IOobject::MUST_READ ) );

    for (int i = 0; i < timeDirs.size(); ++i) {
        runTime.setTime(timeDirs[i], i);
        mesh.readUpdate();

        Info<< "Creating field Y_ref\n" << endl;
        volScalarField Y_ref ( IOobject ( "Y_ref", runTime.timeName(), mesh, IOobject::NO_READ, IOobject::AUTO_WRITE ), mesh, dimensionedScalar("zero", dimless, 0.0) );

        // Flow velocity [m/s]
        scalar v = 10.0;
        scalar t = runTime.time().value();
        // Diffusion coeff [m^2/s]
        scalar D = 5.25e-02;
        // Total source [1/s]
        scalar Q = 1;

        if (t > 1e-12) {
            for (label celli = 0; celli < mesh.nCells(); celli++){
                scalar s = mag(mesh.C()[celli]);
                if (s < 1e-12) {
                    continue;
                }
                scalar z = mesh.C()[celli].z();
                scalar alpha = s*s / (4.0*D);
                scalar beta = v*v / (4.0*D);
                scalar f = 2 * std::sqrt(alpha * beta);
                scalar p = std::sqrt(alpha / t);
                scalar q = std::sqrt(beta * t);
                Y_ref[celli] = Q / (4.0 * constant::mathematical::pi * D * s)
                        * std::exp(v * z / (2.0 * D))
                        * (std::exp(f) * std::erfc(p + q) + std::exp(-f) * std::erfc(p - q));
            }
        }

        Y_ref.write();
    }
    return 0;
}
