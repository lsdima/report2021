#encoding:utf-8
import argparse
import sys
import os
import numpy as np
from scipy import special

import matplotlib.pyplot as plt
import matplotlib.collections as mcol
from matplotlib.legend_handler import HandlerLineCollection, HandlerTuple
from matplotlib.lines import Line2D

def compare(V:np.ndarray, Vr:np.ndarray)->float:
    R=np.abs(V-Vr)
    abs_err=np.linalg.norm(R)
    scale = max(np.linalg.norm(V) ,np.linalg.norm(Vr),1.0e-08)
    rel_err = abs_err / scale
    return rel_err

'''
def residualsLookup(Files:list)->float:
    maxRes=0.
    for f in Files:
        ar=np.loadtxt(f)[-100:]
        if ar.size: maxRes=max(maxRes, ar.mean())
    return maxRes
'''

def myPlot(X:np.ndarray,Y:np.ndarray, Y_ref:np.ndarray,xLabel:str, yLegend:str='Y',yRefLegend:str='Y_ref' ):
    fig, ax = plt.subplots()  
    ax.clear()
    l1,=ax.plot(X,Y)
    l2,=ax.plot(X,Y_ref)
    ax.legend((l1, l2), (yLegend, yRefLegend), loc='upper left', shadow=True)
    ax.set_xlim(X.min(),X.max())  
    ax.grid(1)
    ax.set_xlabel(xLabel)
    #plt.show()
    plt.savefig("XT.png",format="png")

def main_Diff()->float:
    tX={}
    #path_samples= './postProcessing/sample'
    path_samples= './postProcessing/sets'
    for subdir in os.listdir(path_samples):
        t=float(subdir)
        if t>0:
        #print(t)
            fullSubDir= os.path.join(path_samples,subdir)
            for filename in os.listdir(fullSubDir):
                f=os.path.join(fullSubDir,filename)
                #if os.path.isfile(f) and filename.startswith('line_Y'):
                if os.path.isfile(f) and filename.endswith('rho.xy'):
                    # X_Y_H=np.loadtxt(f)
                    # X=X_Y_H[:,0]
                    # Y_H=X_Y_H[:,1]
                    # y=(Y_H.min()+Y_H.max())/2
                    # minVal=min(np.abs(Y_H-y))
                    XY_H=np.loadtxt(f,usecols=(0,1))
                    # Y_H=np.loadtxt(f,usecols=1)
                    y=5.0e-04
                    # y=(XY_H[:,1].min()+XY_H[:,1].max())/2
                    minIndex=np.argmin(np.abs(XY_H[:,1]-y))
                    #print('t={}, X={},X_ref={}, i={}'.format( t,X[minIndex],200.*t, minIndex ))
                    # tX[t]=(X[minIndex],200.*t,minIndex)
                    tX[t]=(XY_H[minIndex,0],200.*t)
    times=np.array(sorted(tX))
    S  =  np.array([tX[k][0] for k in times])
    S_ref=np.array([tX[k][1] for k in times])
    # Indx =np.array([tX[k][2] for k in times])


    myPlot(times,S,S_ref,'t','X(t)','U_left*t')

    cM  =compare(S,S_ref)
    # print('compare(S,Sref)={}'.format(cM))
    return cM

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compute ')

    parser.add_argument('--threshold', type=float, default=2.0e-02, help='Relative error threshold. Default = 1e-02')
    args = parser.parse_args()
    
    errLim=args.threshold

    sol_rel_dif=main_Diff()
    print("mainDiff = {}".format(sol_rel_dif))

    if sol_rel_dif > errLim :
        print('FAIL: resulting Diff ({0:.4e}) > threshold ({1:.4e})'.format(sol_rel_dif, errLim) )
        sys.exit(1)
    else:
        print('PASS')
