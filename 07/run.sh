#!/bin/sh
set -e

blockMesh > blockMesh.log
checkMesh >> blockMesh.log

$1 > solver.log

#postProcess -func sets >sample.log

python scr_07.py --threshold 2.0E-02 > Scr.log
