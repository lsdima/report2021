
# Report 2021 за первый этап

---

## 0) bash helpers to build&run


**В самом начале, пару дней было уделено написанию специальных bash-скриптов,<br> автоматизирующих процесс сборки солвера (включая, базисный OF),<br> и последующий запуск cmake-test.<br> e.g.:**

> **./myScr/installDbnsm.sh** <br>
> Wrong arguments were given! <br>
> Usage: <br>
> 1) To build dbnsm-solver: <br>
> **source ./myScr/installDbnsm.sh -b** <br>
> 2) To install dbnsm-solver: <br>
> **source ./myScr/installDbnsm.sh -i** <br>
> 3) To run validation tests: <br>
> **source ./myScr/installDbnsm.sh -t**<br>

или просто можно установить необходимые env для работы с солвером "из коробки":

> **source $HOME/dbnsm/myScr/setEnv.sh** <br>

#### хотя, полезность этих скриптов, многим представляется спорной, она станет очевидна любому пользователю, если ему придется  запускать dbnsm из "коробки", в чуждой среде, на remote node, w/o sudo.

---

## 1) test cases:
#### от предшествующего автора нам достался большой набор примеров задач <br> (27 штук всего), в разной степени готовности:

	01_1d_forward_wave
	02_1d_backward_wave
	03_2d_steady_laminar_channel
	04_3d_steady_laminar_pipe
	05_3d_cone
	06_interpolator_cube
	07_1d_specie_transport
	08_3d_diffusion_cube_31
	09_3d_diffusion_cube_127
	11_cleaning_detailed_grid
	12_3d_cube_droplet_tracer
	13_1d_riemann_problem
	14_EUV_tracer_test
	15_3d_particles_cube
	16_2d_shock_diffraction
	17_2d_nozzle
	18_2d_forward_facing_step
	19_2d_decaying_vortex
	20_1d_Sn_deposition_test
	21_two_cells
	22_3d_cube_control_loop
	23_2d_low_pressure_outlet
	24_1d_specie_transport_large_dt
	25_test_bc_creation
	26_3d_steady_laminar_pipe_slip
	27_EUV_tracer_test_sphere

***У небольшого числа кейсов - параметры решения были некорректны, для текущей версии солвера. Чтобы выяснить причину неисправности - пришлось слегка повозиться (особенно с № 08)<br>Также, у некоторых отсутствовал (или был не завершён) алгоритм проверки решени на валидность***

---

### К настоящем у моменту:
#### были разобраны и проверены случаи, такие как (7 шт):

	01_1d_forward_wave
	02_1d_backward_wave
	13_1d_riemann_problem
	03_2d_steady_laminar_channel
	04_3d_steady_laminar_pipe
	07_1d_specie_transport
	08_3d_diffusion_cube_31


#### Из них исправлены :

	01_1d_forward_wave
	02_1d_backward_wave

#### Из них доделаны :

	03_2d_steady_laminar_channel
	04_3d_steady_laminar_pipe
	07_1d_specie_transport
	08_3d_diffusion_cube_31

#### В рабочем состоянии: 

	01_1d_forward_wave
	02_1d_backward_wave
	13_1d_riemann_problem
	03_2d_steady_laminar_channel
	<!-- 04_3d_steady_laminar_pipe -->
	<!-- 07_1d_specie_transpor -->
	<!-- 08_3d_diffusion_cube_31 -->
