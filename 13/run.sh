#!/bin/sh

set -e
rm -rf 0 0.0* postProcessing
rm *.log

mkdir -p 0/
cp -r 0.org/* 0/

blockMesh > blockmesh.log
checkMesh >> blockmesh.log
setFields > setfields.log

#$1 2>&1 | tee solver.log
$1 >solver.log

#postProcess -func sampleDict > sample.log

