#encoding: utf-8

from shocktubecalc import sod
import numpy as np

# import sys

# import matplotlib.pyplot as plt
# import matplotlib.collections as mcol
# from matplotlib.legend_handler import HandlerLineCollection, HandlerTuple
# from matplotlib.lines import Line2D


# from matplotlib.animation import FuncAnimation, PillowWriter  
# from itertools import cycle

# from matplotlib import animation
"""

def export_values(fname, vals):
    keys = vals.keys()
    arr = np.array([vals[key] for key in keys]).T
    header = " ".join('{:^17}'.format(str(key)) for key in keys)
    np.savetxt(fname, arr, header=header, fmt='% 1.10e')

def rho_(P,T,molMass=0.02896): return P*molMass/(8.314*T)

def T_(P,rho,molMass=0.02896): return P*molMass/(8.314*rho)

"""

def solution(boolSavetxt=0):
	def export_values(fname, vals):
		keys = vals.keys()
		arr = np.array([vals[key] for key in keys]).T
		header = " ".join('{:^17}'.format(str(key)) for key in keys)
		np.savetxt(fname, arr, header=header, fmt='% 1.10e')

	def rho_(P,T,molMass=0.02896): return P*molMass/(8.314*T)

	def T_(P,rho,molMass=0.02896): return P*molMass/(8.314*rho)

	Gamma=1.4
	Npts=2000

	x_min=-5.
	x_max=5.

	x_0_contact=0.
	# 
	t_end=0.015
	dt=0.001

	P_0_l=1.0e+05
	T_0_l=348.432
	rho_0_l=rho_(P_0_l,T_0_l) #->T=348.432
	U_0_l=0.
	#
	P_0_r=1.0e+04
	T_0_r=278.746
	rho_0_r=rho_(P_0_r,T_0_r)#0.125 #->T=278.746
	U_0_r=0.

	times=np.arange(0.,t_end+dt,dt)
	VALUES={}

	for tm in times:
		# positions, regions, values = sod.solve(left_state=(P_0_l, rho_0_l, U_0_l), right_state=(P_0_r, rho_0_r, U_0_r), geometry=(x_min, x_max, x_0_contact), t, gamma, npts)
		positions, regions, values = sod.solve(left_state=(P_0_l, rho_0_l, U_0_l), right_state=(P_0_r, rho_0_r, U_0_r),
                                       geometry=(x_min, x_max, x_0_contact), t=tm, gamma=Gamma, npts=Npts)


		T = T_(values['p'],values['rho'])
		c = np.sqrt(Gamma * values['p'] / values['rho'])
		M = values['u'] / c
		e=c*c/(Gamma*(Gamma-1))
		h=c*c/(Gamma-1)
		E = e*values['rho'] + 0.5*values['rho']*values['u']**2

		# export calculated values only
		# export_values(fname='sod_calculated_only.txt', vals=values)

		# export all values
		values['e'] = e
		values['E'] = E
		values['c'] = c
		values['M'] = M
		values['T'] = T
		values['h'] = h

		if boolSavetxt: #columns: x,p,rho,u,e,E,c,M,T,h			
			export_values(fname='{0:.3f}.ref'.format(tm), vals=values)

		VALUES[tm]=values
	return VALUES

if __name__ == '__main__':
	# 
	valueS=solution(1)
