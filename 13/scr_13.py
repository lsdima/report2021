#encoding:utf-8
import argparse
import sys
import os
import numpy as np
from scipy import special
# from . import sod


import matplotlib.pyplot as plt
import matplotlib.collections as mcol
from matplotlib.legend_handler import HandlerLineCollection, HandlerTuple
from matplotlib.lines import Line2D

def compare(V:np.ndarray, Vr:np.ndarray)->float:
    R=np.abs(V-Vr)
    abs_err=np.linalg.norm(R)
    scale = max(np.linalg.norm(V) ,np.linalg.norm(Vr),1.0e-08)
    rel_err = abs_err / scale
    return rel_err

'''
def residualsLookup(Files:list)->float:
    maxRes=0.
    for f in Files:
        ar=np.loadtxt(f)[-100:]
        if ar.size: maxRes=max(maxRes, ar.mean())
    return maxRes
'''

def myPlot(X:np.ndarray,Y:np.ndarray, Y_ref:np.ndarray,xLabel:str, yLegend:str='Y',yRefLegend:str='Y_ref' ):
    fig, ax = plt.subplots()  
    ax.clear()
    l1,=ax.plot(X,Y)
    l2,=ax.plot(X,Y_ref)
    ax.legend((l1, l2), (yLegend, yRefLegend), loc='upper left', shadow=True)
    ax.set_xlim(X.min(),X.max())  
    ax.grid(1)
    ax.set_xlabel(xLabel)
    #plt.show()
    plt.savefig("XT.png",format="png")

def main_Diff()->float:
    
    tX={}
    path_samples= './postProcessing/sample'
    
    sampList=[os.path.join(path_samples,di,os.listdir(os.path.join(path_samples,di))[0]) for di in sorted(os.listdir(path_samples))]
    
    times=[subdir for subdir in sorted(os.listdir(path_samples))]

    refList=sorted([item for item in os.listdir('./') if os.path.isfile(item) and item.endswith('ref')])
    assert len(sampList)==len(refList), "len(sampList)!=len(refList) ! "

    for i in range(len(sampList)):
        
        SMP=np.loadtxt(sampList[i]) #columns:   x, T, mag(U), p, rho
        X = SMP[:,0]
        T = SMP[:,1]
        U = SMP[:,2]
        P = SMP[:,3]
        rho=SMP[:,4]
        #
        REF=np.loadtxt(refList[i],skiprows=1) #columns:    x,p,rho,u,e,E,c,M,T,h
        #                                                  0 1  2  3 4 5 6 7 8 9
        refX = REF[:,0]
        refT = REF[:,8]
        refU = REF[:,3]
        refP = REF[:,1]
        refRho=REF[:,2]
        assert len(X)==len(refX), "len(X)!=len(refX) !"
        assert compare(X,refX)<= np.finfo(np.float32).eps , "X does not coincide with refX !"

        fig, ax = plt.subplots(5)
        ax.clear()
        l1,=ax.plot(X,T)
        l2,=ax.plot(X,refT)

        ax.legend((l1, l2), ("T", "T_ref"), loc='upper left', shadow=True)
        # ax.set_xlim(X.min(),X.max())  
        ax.grid(1)
        ax.set_xlabel(times[i])
        #plt.show()
        plt.savefig(times[i],format="png")




    # sampList=sampList[1:]ё
    # refList=refList[1:]


    # minVal=min(np.abs(Y_H-y))
    # XY_H=np.loadtxt(f,usecols=(0,1))
    # Y_H=np.loadtxt(f,usecols=1)
    # y=5.0e-04
    # y=(XY_H[:,1].min()+XY_H[:,1].max())/2
    # minIndex=np.argmin(np.abs(XY_H[:,1]-y))
    #print('t={}, X={},X_ref={}, i={}'.format( t,X[minIndex],200.*t, minIndex ))
    # tX[t]=(X[minIndex],200.*t,minIndex)
    # tX[t]=(XY_H[minIndex,0],200.*t)
    # times=np.array(sorted(tX))
    # S  =  np.array([tX[k][0] for k in times])
    # S_ref=np.array([tX[k][1] for k in times])
    # Indx =np.array([tX[k][2] for k in times])


    # myPlot(times,S,S_ref,'t','X(t)','U_left*t')


 
    # cM  =compare(S,S_ref)
    # print('compare(S,Sref)={}'.format(cM))
    # return cM

if __name__ == '__main__':
    """
    parser = argparse.ArgumentParser(description='Compute ')

    parser.add_argument('--threshold', type=float, default=2.0e-02, help='Relative error threshold. Default = 1e-02')
    args = parser.parse_args()
    
    errLim=args.threshold

    sol_rel_dif=main_Diff()
    print("mainDiff = {}".format(sol_rel_dif))

    if sol_rel_dif > errLim :
        print('FAIL: resulting Diff ({0:.4e}) > threshold ({1:.4e})'.format(sol_rel_dif, errLim) )
        sys.exit(1)
    else:
        print('PASS')
    """
    main_Diff()


