#!/usr/bin/env python3

import argparse
import math
import os
import sys

import numpy as np
from scipy.integrate import odeint
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Script for plotting droplet trajectories and comparing to independent calculation')
parser.add_argument('--sampling-time', type=str, metavar='DIR', 
                    help='Time name from which to read sampled proprties. If unable to read, will use some default constants.')
args = parser.parse_args()

if args.sampling_time:
    TIME = args.sampling_time
else:
    TIME = '0'

def plot_droplet_trajectory_z(droplet_id):
    filename = os.path.join('droplets', 'droplet-{}.xy'.format(droplet_id))
    x = np.loadtxt(filename, skiprows=1, ndmin=2)
    print(droplet_id, x.shape)
    plt.plot(x[:,1], x[:,3], 'o', label='Droplet {}'.format(droplet_id))
        
# for droplet_id in (1, 10):
#     plot_droplet_trajectory_z(droplet_id)

d = 34e-06
x0 = np.asarray((0.01, 0, 0))
rho_droplet = 7000.0
m = 1/6 * math.pi * d**3 * rho_droplet
R = 8.314

path = os.path.join('postProcessing', 'sets', TIME) 

filename_U = os.path.join(path, 'droplet_path_U.xy')
try:
    sampled_U = np.loadtxt(filename_U)
    U = interp1d(sampled_U[:,0], sampled_U[:,1:4], axis=0, fill_value='extrapolate')
    print('Loaded', filename_U)
except Exception as e:
    print('Warning: Could not load sampled velocity:', e, '\n    Using constant value instead')
    def U(x):
        return np.asarray((0, 0, 100))

scalars_loaded = False        
try:
    # filename_scalars = os.path.join(path, 'droplet_path_p_T_rho_mu_molar_mass.xy')
    # p, T, rho, mu, molar_mass
    # T, molar_mass, mu, p, rho
    # -0.006896552 	299.8028 	0.0020152833 	8.946618e-06 	74.999991 	6.0635588e-05
    # 1,2,3,4,5
    # 4,1,5,3,2
    filename_scalars = os.path.join(path, 'droplet_path_T_molar_mass_mu_p_rho.xy')
    sampled_scalars = np.loadtxt(filename_scalars)
    # p   = interp1d(sampled_scalars[:,0], sampled_scalars[:,1], fill_value='extrapolate')
    p   = interp1d(sampled_scalars[:,0], sampled_scalars[:,4], fill_value='extrapolate')
    T   = interp1d(sampled_scalars[:,0], sampled_scalars[:,1], fill_value='extrapolate')
    rho = interp1d(sampled_scalars[:,0], sampled_scalars[:,5], fill_value='extrapolate')
    eta  = interp1d(sampled_scalars[:,0], sampled_scalars[:,3], fill_value='extrapolate')
    molar_mass = interp1d(sampled_scalars[:,0], sampled_scalars[:,2], fill_value='extrapolate')
    print('Loaded', filename_scalars)
    scalars_loaded = True
except Exception as e:
    print('Warning: Could not load sampled scalars:', e)

"""
if not scalars_loaded:
    try:
        filename_scalars = os.path.join(path, 'droplet_path_p_T.xy')
        sampled_scalars = np.loadtxt(filename_scalars)
        p   = interp1d(sampled_scalars[:,0], sampled_scalars[:,1], fill_value='extrapolate')
        T   = interp1d(sampled_scalars[:,0], sampled_scalars[:,2], fill_value='extrapolate')
        def molar_mass(x):
            return 0.002
        def rho(x):
            return p(x) * molar_mass(x) / (R * T(x))
        def eta(x):
            return 8.95e-06
        print('Loaded', filename_scalars)
        scalars_loaded = True
    except Exception as e:
        print('Warning: Could not load sampled scalars:', e)
"""
if not scalars_loaded:
    print('Using constant scalar parameters')
    def p(x):
        return 75.0
    def T(x):
        return 300.0
    def rho(x):
        return 6.0596e-05
    def molar_mass(x):
        return 0.002
    def eta(x):
        return 8.95e-06

def k_drag(T, mu, d, p, eta, rho_gas, u):
    Kn    = math.sqrt(0.5 * math.pi * R * T / mu) * eta / (d * p * 0.491)
    Cc_Kn = Kn * ( 1.141 + 0.506 * math.exp(-0.85 / Kn) ) + 1.0
    Re    = d * u * rho_gas / eta
    Cd_Re = 1.0 + 0.173 * math.exp( 0.657 * math.log(Re) ) + 0.01721 * Re / (1.0 + 16300.0 * math.exp(-1.09 * math.log(Re)) )
    k_drag = 3.0 * math.pi * d * eta / Cc_Kn * Cd_Re ;
    return k_drag

def func(y, t):
    x = y[0:3]
    v = y[3:6]
    U_loc = U(x[0])
    F = k_drag(T(x[0]), molar_mass(x[0]), d, p(x[0]), eta(x[0]), rho(x[0]), np.linalg.norm(U_loc - v)) * (U_loc - v)
    return np.concatenate((v, F/m))

# t = np.linspace(0, 0.0005, 200)
t = np.linspace(0, 0.0015, 200)
# print(t)

y0 = np.concatenate((x0, v0))
z = odeint(func, y0, t)

plt.plot(z[:,0], z[:,2], '-', label='Python')

plt.xlabel('x')
plt.ylabel('dz')
plt.legend()
plt.grid()
plt.savefig('droplets.png')

