#!/bin/sh

set -e

##blockMesh > blockMesh.log

gmshToFoam ./meshing/mesh.msh > blockMesh.log
checkMesh >> blockMesh.log

# Run the solver 
$1 > solver.log


postProcess -func sample -latestTime > sample.log


scripts/residuals.sh

python scr_04.py --resLim 1.0E-02 --threshold 3.0E-00

