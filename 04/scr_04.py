#encoding:utf-8
import argparse
import sys
import os
import numpy as np


def compare(V:np.ndarray, Vr:np.ndarray)->float:
    # np.linalg.norm is for L2 norm:  np.sqrt(np.sum(Xi**2))
    # V  = np.apply_along_axis(np.linalg.norm, 1, U_result) #vector of | U_result(Xi) |
    # Vr = np.apply_along_axis(np.linalg.norm, 1, U_ref)    #vector of | U_ref(Xi) |
    cosOhi=np.vdot(V,Vr)/()
    R=np.abs(V-Vr)
    abs_err=np.linalg.norm(R)
    # scale=( np.linalg.norm(Vr) + np.linalg.norm(V) )/2
    scale = np.max([np.linalg.norm(V) ,np.linalg.norm(Vr),1.0e-08])
    rel_err = abs_err /scale
    return rel_err
    # return abs_err

def residualsLookup(Files:list)->float:
    maxRes=0.
    for f in Files:
        ar=np.loadtxt(f)[-100:]
        if ar.size: maxRes=max(maxRes, ar.mean())
    return maxRes

# def U_y(y:float, gradP:float, Mu:float,h:float)->float:return 1./(2*Mu)*y*(y-h)*gradP
def U_r(r:float, gradP:float, Mu:float,R:float)->float:
    return (R*R-r*r)*gradP/(4*Mu)


def main_Diff()->float:
    lineX_TP=np.loadtxt('./postProcessing/sample/0.012/lineX_T_p_rho.xy')
    lineX_U=np.loadtxt('./postProcessing/sample/0.012/lineX_U.xy')
    lineY_TP=np.loadtxt('./postProcessing/sample/0.012/lineY_T_p_rho.xy')
    lineY_U=np.loadtxt('./postProcessing/sample/0.012/lineY_U.xy')
    lineZ_TP=np.loadtxt('./postProcessing/sample/0.012/lineZ_T_p_rho.xy')
    lineZ_U=np.loadtxt('./postProcessing/sample/0.012/lineZ_U.xy')


    # h=lineY_U[:,0][-1]+lineY_U[:,0][0]
    R=0.05

    fluidProps={'mu':8.9507e-06,'rho':lineX_TP[:,3].mean()}

    Re=fluidProps['rho'] * lineX_U[:,1].max() *R / fluidProps['mu']

    assert Re<2100 , "tis NOT a laminar flow !!!"

    assert lineX_TP.shape[0] >20, "length of lineX_TP DOES matter ! "

    dP_dX=(lineX_TP[-10][2]-lineX_TP[10][2])/(lineX_TP[-10][0]-lineX_TP[10][0])

    Y=lineY_U[:,0]
    Uy=lineY_U[:,1]
    Z=lineZ_U[:,0]
    Uz=lineZ_U[:,1]

    # th_Uy=np.zeros(lineY_U[:,0].size)
    th_Uy=np.array([ U_r( y, dP_dX,fluidProps['mu'], R ) for y in Y ])
    th_Uz=np.array([ U_r( z, dP_dX,fluidProps['mu'], R ) for z in Z ])

    print('maxUy= {}'.format(Uy.max()))
    print('maxUz= {}'.format(Uz.max()))
    print('dP_dX={}'.format(dP_dX))
    print('rho = {}'.format(fluidProps['rho']))
    print('compare(Uy,th_Uy)={}'.format(compare(Uy,th_Uy)))
    print('compare(Uz,th_Uz)={}'.format(compare(Uz,th_Uz)))

    return max(compare(Uy,th_Uy),compare(Uz,th_Uz))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compute ')
    # parser.add_argument('file', nargs=2, help='Data file to compare')
    parser.add_argument('--resLim', type=float, default=5.0e-04, help='max relative Residual threshold. Default = 1e-03')
    parser.add_argument('--threshold', type=float, default=2.0e-03, help='Relative error threshold. Default = 1e-02')
    args = parser.parse_args()
    
    errLim=args.threshold
    maxResThr= args.resLim

    lsFiles=[ i for i in os.listdir("./") if ( i.endswith('rel.txt') and i.startswith('R') ) ]


    maxResidual= residualsLookup(lsFiles)

    # lineX_TP='../postProcessing/sample/0.02/lineX_T_p_rho.xy'
    # lineX_U='../postProcessing/sample/0.02/lineX_U.xy'
    # lineY_TP='../postProcessing/sample/0.02/lineY_T_p_rho.xy'
    # lineY_U='../postProcessing/sample/0.02/lineY_U.xy'
    sol_rel_dif=main_Diff()
    print("mainDiff = {}".format(sol_rel_dif))
    print("maxResidual = {}".format(maxResidual))

    if maxResidual > maxResThr :
        print('FAIL: SOLUTION SEEMS TO HAVE NOT CONVERGED! Resulting maximal residual ({0:.4e}) > resLim ({1:.4e})'.format(maxResidual, maxResThr) )
        sys.exit(1)

    elif sol_rel_dif > errLim :
        print('FAIL: resulting Diff ({0:.4e}) > threshold ({1:.4e})'.format(sol_rel_dif, errLim) )
        sys.exit(1)
    else:
        print('PASS')
