#!/usr/bin/env python3

import numpy as np
#import matplotlib
#matplotlib.use('AGG')
import matplotlib.pyplot as plt
import os
from scipy import special

PLOTS = (
    ('x_axis',         'X axis (z = 0)',    'x', (-0.2, 0.2)),
    ('x_axis_shifted', 'X axis (z = 0.15)', 'x', (-0.2, 0.2)),
    ('z_axis',         'Z axis',            'z', (-0.1, 0.5)),
)

CASES = (
    ('1', 'N =  31, dt = 1e-03'),
    ('1b', 'N =  31, dt = 1e-03, zero BC'),
    ('2', 'N =  31, dt = 1e-04'),
    ('3', 'N =  31, dt = 1e-05'),
    ('4', 'N =  63, dt = 1e-03'),
    ('5-par', 'N = 127, dt = 1e-03'),
)


def Y_ref(x, y, z, t, D, v):
    s = np.sqrt(x*x + y*y + z*z)
    alpha = s*s / (4*D)
    beta = v*v / (4*D)
    f = 2 * np.sqrt(alpha * beta)
    p = np.sqrt(alpha / t)
    q = np.sqrt(beta * t)
    return 1 / (4 * np.pi * D * s) \
        * np.exp(v * z / (2 * D)) \
        * (np.exp(f) * special.erfc(p + q) + np.exp(-f) * special.erfc(p - q))

for sample, plot_title, xlabel, xlim in PLOTS:
    #t = 0.015
    t = 0.06
    D = 5.25e-02
    v = 10.0
    N = 1000
    z_r = 0.15
    print(sample)
    plt.clf()
    ymax = 0.0
    for case_dir, case_title in CASES:
        print(case_dir)
        x, Y_Sn, _ = np.loadtxt(os.path.join(case_dir, 'postProcessing', 'sample', '0.015', sample + '_Y_Sn_Y_ref.xy'), unpack=True)
        ymax = max((np.max(Y_Sn), ymax))
        plt.plot(x, Y_Sn,     label=case_title)
    if sample == 'x_axis':
        x1 = np.linspace(-0.5, 0.5, N)
        y1 = np.zeros(N)
        z1 = np.zeros(N)
        x_r = x1
    elif sample == 'x_axis_shifted':
        x1 = np.linspace(-0.5, 0.5, N)
        y1 = np.zeros(N)
        z1 = np.zeros(N) + z_r
        x_r = x1
    elif sample == 'z_axis':
        x1 = np.zeros(N)
        y1 = np.zeros(N)
        z1 = np.linspace(-0.5, 0.5, N)
        x_r = z1
    else:
        raise ValueError('Unknown sample type ' + sample)
    Y_r = Y_ref(x1, y1, z1, t, D, v)
    plt.plot(x_r, Y_r*3e-05, label='Ref')
    plt.grid()
    plt.xlabel(xlabel)
    plt.ylabel('Y_Sn')
    plt.legend()
    plt.xlim(xlim)
    plt.title(plot_title)
    plt.savefig(sample + '.png')
    plt.yscale('log')
    plt.ylim((1e-09, ymax*2))
    plt.savefig(sample + '_log' + '.png')


