#!/bin/sh
set -e

blockMesh > blockMesh.log
checkMesh >> blockMesh.log
topoSet > topoSet.log

$1 > solver.log

postProcess -func sample -latestTime > sample.log


scripts/residuals.sh

python scr_08.py --resLim 1.0E-02 --threshold 3.0E-00 >Scr.log
